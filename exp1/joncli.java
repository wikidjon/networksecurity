import java.io.*;
import java.net.*;
import java.util.*;
class joncli
{	
	public static void main(String args[])throws Exception
	{
			Scanner dis = new Scanner(System.in);
			boolean send;
			String str;
			byte b[] = new byte[1500];
			byte bb[] = new byte[1500];
			send = false;
			while(true)
			{
			Socket soc = new Socket(InetAddress.getLocalHost(),Integer.parseInt(args[0]));
			InputStream in = soc.getInputStream();
			OutputStream out = soc.getOutputStream();
			if(send)
			{
				str = dis.nextLine();
				System.out.println("CLIENT:\n " +str);
				if(Integer.parseInt(args[1]) > 26)		
					str = cipher(str,(Integer.parseInt(args[1]) - 26));
				else
					str = cipher(str,Integer.parseInt(args[1]));
				b = str.getBytes();
				
				out.write(b);
				send = false;
			}
			else
			{
				int d = in.read(bb);
				String str1 = new String(bb,0,d);
				System.out.println("SERVER:\n " +str1);	
				if(Integer.parseInt(args[1]) > 26)		
					str1 = decipher(str1,(Integer.parseInt(args[1]) - 26));
				else
					str1 = decipher(str1,Integer.parseInt(args[1]));
				System.out.println("SERVER:\n " +str1);	
				send = true;
			}
		}
	}
	
	static String cipher(String str,int shift){
		int i=0;
		char[] chararray = str.toCharArray();	
		
		for(i=0;i<chararray.length;i++){
			char cha =(char)( chararray[i] + shift );
			chararray[i] = cha;
		}
		str = String.valueOf(chararray);
		return str;
	}
	
	static String decipher(String str,int shift){
		int i=0;
		char[] chararray = str.toCharArray();	
		
		for(i=0;i<chararray.length;i++){
			char cha =(char)( chararray[i] - shift );
			chararray[i] = cha;
		}
		str = String.valueOf(chararray);
		return str;
	}-	
}