import java.net.*;
import java.io.*;

public class cae_ser { 
  public void run() {
	try {
		int serverPort = 4020;
		String line = "";
		ServerSocket serverSocket = new ServerSocket(serverPort);
		serverSocket.setSoTimeout(10000); 
		while(true) {
			System.out.println("Waiting for client on port " + serverSocket.getLocalPort() + "..."); 

			Socket server = serverSocket.accept();
			System.out.println("Just connected to " + server.getRemoteSocketAddress()); 
			while(line != "bye"){
			PrintWriter toClient = 
				new PrintWriter(server.getOutputStream(),true);
			BufferedReader fromClient =
				new BufferedReader(
						new InputStreamReader(server.getInputStream()));
			line = fromClient.readLine();
			System.out.println("Server received: " + line); 
			}
		}
	}
	catch(UnknownHostException ex) {
		ex.printStackTrace();
	}
	catch(IOException e){
		e.printStackTrace();
	}
  }
	
  public static void main(String[] args) {
		cae_ser srv = new cae_ser();
		srv.run();
  }
}