import java.io.*;
import java.net.*;
import java.util.*;
class jonser
{
public static void main(String args[])throws Exception
{
	ServerSocket s= new ServerSocket(Integer.parseInt(args[0]));
	Scanner dis = new Scanner(System.in);
	
	boolean send;	
	String str;
	byte b[] = new byte[1500];
	byte bb[] = new byte[1500];
	send = true;
	while(true)
	{
	 Socket soc = s.accept();
	 InputStream in = soc.getInputStream();
	 OutputStream out=soc.getOutputStream();
	 if (send)
	 {
		str=dis.nextLine();
		System.out.println("SERVER:\n" +str);
		if(Integer.parseInt(args[1]) > 26)		
			str = cipher(str,Integer.parseInt(args[1]) - 26);
		else
			str = cipher(str,Integer.parseInt(args[1]));
		bb=str.getBytes();
		
		out.write(bb);
		send=false;
	 }
	 else
	 {
		int d=in.read(b);
		String str1=new String(b,0,d);
		if(Integer.parseInt(args[1]) > 26)		
			str1 = decipher(str1,Integer.parseInt(args[1]) - 26);
		else
			str1 = decipher(str1,Integer.parseInt(args[1]));
		System.out.println("CLIENT;\n"+str1);
		send =true;
	 }
}
}

static String cipher(String str,int shift){
		int i=0;
		char[] chararray = str.toCharArray();	
		
		for(i=0;i<chararray.length;i++){
			char cha =(char)( chararray[i] + shift );
			chararray[i] = cha;
		}
		str = String.valueOf(chararray);
		return str;
}

static String decipher(String str,int shift){
		int i=0;
		char[] chararray = str.toCharArray();	
		
		for(i=0;i<chararray.length;i++){
			char cha =(char)( chararray[i] - shift );
			chararray[i] = cha;
		}
		str = String.valueOf(chararray);
		return str;
}

}